#!/bin/bash

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

cd $SCRIPTPATH

# cd stellantis
# npm run build
# cd ..

cd tps
npm run build
cd ..

cd vwfs
npm run build
cd ..