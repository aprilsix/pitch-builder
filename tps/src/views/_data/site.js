module.exports = {
  title: "April Six",
  description:
    "Thank you for considering April Six in your review of potential agency partners.",
  url: "https://www.aprilsix.com",
  client: "VW TPS", // client name here
  contact: {
    name: "Samantha Smith",
    email: "samantha.smith@aprilsix.com",
    phone: "+44 (0)7739 819269",
    phonelink: "+447739819269",
    address: "Parley Green Lane <br>Christchurch <br>Bournemouth <br>BH23 6BB",
  },
  dir: "tps",
  author: "Rob Wills",
  meta_data: {
    twitter: "@aprilsix",
    default_social_image: "/static/default_social_image.jpg",
  },
  env: process.env.ELEVENTY_ENV === "production",
};
