const eleventyNavigationPlugin = require("@11ty/eleventy-navigation");
const svgContents = require("eleventy-plugin-svg-contents");
const yaml = require("js-yaml");

module.exports = function (eleventyConfig) {
    // add dir that need to be passed through to the site
    eleventyConfig.addWatchTarget("./src/scss/");

    eleventyConfig.addPassthroughCopy({ "./src/css": "css" });
    eleventyConfig.addPassthroughCopy({ "./src/static": "static" });
    eleventyConfig.addPassthroughCopy({ "./src/js": "js" });
    eleventyConfig.addPassthroughCopy({ "./src/fonts": "fonts" });
    eleventyConfig.addPassthroughCopy({ "./src/img": "img" });
    eleventyConfig.addPassthroughCopy({ "./src/video": "video" });
    eleventyConfig.addPassthroughCopy({ "./src/docs": "docs" });
    eleventyConfig.addPassthroughCopy({ "./src/nimble": "nimble" });
    eleventyConfig.addPassthroughCopy({ "./src/.htaccess": ".htaccess" });
    eleventyConfig.addPassthroughCopy({ "./src/.htpasswd": ".htpasswd" });
    eleventyConfig.addPassthroughCopy({ "./src/dpath.php": "dpath.php" });

    // add naviagtion plugin
    eleventyConfig.addPlugin(eleventyNavigationPlugin);
    eleventyConfig.addPlugin(svgContents);

    return {
        dir: {
            input: "src/views",
            output: "public",
            includes: "_includes",
            layouts: "_layouts",
        },
    };
};
