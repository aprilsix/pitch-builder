# Welcome to the April Six Pitch Builder repo

## TODO!

Improvments to consider for future sites

Deployment method needs an extra step to prevent downtime when building the site on prod. It should build to a separate folder and then the folder should be swapped with the live one.

A rebuild of the site would be nice. Currently there is a lot of clutter in the sass especially with bootstrap, plus I'm not a fan of the BEM style that my predecessor implemented.

CMS-able, we should look at adopting services such as strapi to make the content cms driven allowing the pitch team to upload their own content. By doing this the dev team only need to worry about dev ops and we don't need extra storage on web01.

## Quick start

```
git clone git@bitbucket.org:aprilsix/pitch-builder.git

cd pitch-builder

# cd to the subdirectory you want to work on

npm install

npm start
```

The 11ty web server will start and you can view the site

You can download any uncommited large files from the webserver using the following command from the root directory.

```
sh pull_large_files.sh
```

This script assumes you have a ssh config set up with **aprilsix-web01** and have **rsync** installed.

# Deployment instructions

Access the web server and navigate to the repository directory and then the subdirectory of the site you want to update, then run the following command

```
npm run build
```

This will rebuild the site from the src folder into the public folder.

Large files aren't stored inside the repo so you'll transfer them from your local machine to the web server. You can do this with the following command from the root directory.

```
sh push_large_files.sh
```

This script assumes you have a ssh config set up with **aprilsix-web01** and have **rsync** installed.

# Site build information

The repo contains several sites, each one a variant of the other.

They are all built with 11ty. Instead of the standard set up the template files will be found inside src/views making the source and templating directories cleaner and easier to work with. Because of this set up if you wish to have files copied over from src to public you will need to specify the routes for each file or folder in the .eleventy.js config file.

```
    eleventyConfig.addPassthroughCopy({ "./src/css": "css" });
    eleventyConfig.addPassthroughCopy({ "./src/static": "static" });
    eleventyConfig.addPassthroughCopy({ "./src/js": "js" });
```
The css is built with both pre and post processing. The preprocessor is SASS and the files can be found in the scss directory. Tailwind is employed as a post processor. Bootstrap is employed as a framework.

This set up is legacy and going forward I'd be keen to change this to integrate tailwind into sass and remove bootstrap. Opinionated frameworks introduce bloat and are not as essential in a post css flex and grid world.

Tailwind is built and placed ahead of the sass files, which doesn't make sense as you'd want the utility classes to be last in the final css. Overall the css can be cut down significantly and cleanly written.