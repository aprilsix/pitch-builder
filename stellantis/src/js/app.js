// console.log("hello js");

/*! modernizr 3.6.0 (Custom Build) | MIT *
 * https://modernizr.com/download/?-backdropfilter-cssfilters-setclasses !*/
!(function (e, n, t) {
  function r(e, n) {
    return typeof e === n;
  }
  function s() {
    var e, n, t, s, o, i, l;
    for (var a in S)
      if (S.hasOwnProperty(a)) {
        if (
          ((e = []),
          (n = S[a]),
          n.name &&
            (e.push(n.name.toLowerCase()),
            n.options && n.options.aliases && n.options.aliases.length))
        )
          for (t = 0; t < n.options.aliases.length; t++)
            e.push(n.options.aliases[t].toLowerCase());
        for (s = r(n.fn, "function") ? n.fn() : n.fn, o = 0; o < e.length; o++)
          (i = e[o]),
            (l = i.split(".")),
            1 === l.length
              ? (Modernizr[l[0]] = s)
              : (!Modernizr[l[0]] ||
                  Modernizr[l[0]] instanceof Boolean ||
                  (Modernizr[l[0]] = new Boolean(Modernizr[l[0]])),
                (Modernizr[l[0]][l[1]] = s)),
            C.push((s ? "" : "no-") + l.join("-"));
      }
  }
  function o(e) {
    var n = _.className,
      t = Modernizr._config.classPrefix || "";
    if ((x && (n = n.baseVal), Modernizr._config.enableJSClass)) {
      var r = new RegExp("(^|\\s)" + t + "no-js(\\s|$)");
      n = n.replace(r, "$1" + t + "js$2");
    }
    Modernizr._config.enableClasses &&
      ((n += " " + t + e.join(" " + t)),
      x ? (_.className.baseVal = n) : (_.className = n));
  }
  function i() {
    return "function" != typeof n.createElement
      ? n.createElement(arguments[0])
      : x
      ? n.createElementNS.call(n, "http://www.w3.org/2000/svg", arguments[0])
      : n.createElement.apply(n, arguments);
  }
  function l(e, n) {
    return !!~("" + e).indexOf(n);
  }
  function a(e) {
    return e
      .replace(/([a-z])-([a-z])/g, function (e, n, t) {
        return n + t.toUpperCase();
      })
      .replace(/^-/, "");
  }
  function u(e, n) {
    return function () {
      return e.apply(n, arguments);
    };
  }
  function f(e, n, t) {
    var s;
    for (var o in e)
      if (e[o] in n)
        return t === !1
          ? e[o]
          : ((s = n[e[o]]), r(s, "function") ? u(s, t || n) : s);
    return !1;
  }
  function p(e) {
    return e
      .replace(/([A-Z])/g, function (e, n) {
        return "-" + n.toLowerCase();
      })
      .replace(/^ms-/, "-ms-");
  }
  function c(n, t, r) {
    var s;
    if ("getComputedStyle" in e) {
      s = getComputedStyle.call(e, n, t);
      var o = e.console;
      if (null !== s) r && (s = s.getPropertyValue(r));
      else if (o) {
        var i = o.error ? "error" : "log";
        o[i].call(
          o,
          "getComputedStyle returning null, its possible modernizr test results are inaccurate"
        );
      }
    } else s = !t && n.currentStyle && n.currentStyle[r];
    return s;
  }
  function d() {
    var e = n.body;
    return e || ((e = i(x ? "svg" : "body")), (e.fake = !0)), e;
  }
  function m(e, t, r, s) {
    var o,
      l,
      a,
      u,
      f = "modernizr",
      p = i("div"),
      c = d();
    if (parseInt(r, 10))
      for (; r--; )
        (a = i("div")), (a.id = s ? s[r] : f + (r + 1)), p.appendChild(a);
    return (
      (o = i("style")),
      (o.type = "text/css"),
      (o.id = "s" + f),
      (c.fake ? c : p).appendChild(o),
      c.appendChild(p),
      o.styleSheet
        ? (o.styleSheet.cssText = e)
        : o.appendChild(n.createTextNode(e)),
      (p.id = f),
      c.fake &&
        ((c.style.background = ""),
        (c.style.overflow = "hidden"),
        (u = _.style.overflow),
        (_.style.overflow = "hidden"),
        _.appendChild(c)),
      (l = t(p, e)),
      c.fake
        ? (c.parentNode.removeChild(c), (_.style.overflow = u), _.offsetHeight)
        : p.parentNode.removeChild(p),
      !!l
    );
  }
  function y(n, r) {
    var s = n.length;
    if ("CSS" in e && "supports" in e.CSS) {
      for (; s--; ) if (e.CSS.supports(p(n[s]), r)) return !0;
      return !1;
    }
    if ("CSSSupportsRule" in e) {
      for (var o = []; s--; ) o.push("(" + p(n[s]) + ":" + r + ")");
      return (
        (o = o.join(" or ")),
        m(
          "@supports (" + o + ") { #modernizr { position: absolute; } }",
          function (e) {
            return "absolute" == c(e, null, "position");
          }
        )
      );
    }
    return t;
  }
  function v(e, n, s, o) {
    function u() {
      p && (delete j.style, delete j.modElem);
    }
    if (((o = r(o, "undefined") ? !1 : o), !r(s, "undefined"))) {
      var f = y(e, s);
      if (!r(f, "undefined")) return f;
    }
    for (
      var p, c, d, m, v, g = ["modernizr", "tspan", "samp"];
      !j.style && g.length;

    )
      (p = !0), (j.modElem = i(g.shift())), (j.style = j.modElem.style);
    for (d = e.length, c = 0; d > c; c++)
      if (
        ((m = e[c]),
        (v = j.style[m]),
        l(m, "-") && (m = a(m)),
        j.style[m] !== t)
      ) {
        if (o || r(s, "undefined")) return u(), "pfx" == n ? m : !0;
        try {
          j.style[m] = s;
        } catch (h) {}
        if (j.style[m] != v) return u(), "pfx" == n ? m : !0;
      }
    return u(), !1;
  }
  function g(e, n, t, s, o) {
    var i = e.charAt(0).toUpperCase() + e.slice(1),
      l = (e + " " + E.join(i + " ") + i).split(" ");
    return r(n, "string") || r(n, "undefined")
      ? v(l, n, s, o)
      : ((l = (e + " " + k.join(i + " ") + i).split(" ")), f(l, n, t));
  }
  function h(e, n, r) {
    return g(e, t, t, n, r);
  }
  var C = [],
    S = [],
    w = {
      _version: "3.6.0",
      _config: {
        classPrefix: "",
        enableClasses: !0,
        enableJSClass: !0,
        usePrefixes: !0,
      },
      _q: [],
      on: function (e, n) {
        var t = this;
        setTimeout(function () {
          n(t[e]);
        }, 0);
      },
      addTest: function (e, n, t) {
        S.push({ name: e, fn: n, options: t });
      },
      addAsyncTest: function (e) {
        S.push({ name: null, fn: e });
      },
    },
    Modernizr = function () {};
  (Modernizr.prototype = w), (Modernizr = new Modernizr());
  var _ = n.documentElement,
    x = "svg" === _.nodeName.toLowerCase(),
    b = "CSS" in e && "supports" in e.CSS,
    P = "supportsCSS" in e;
  Modernizr.addTest("supports", b || P);
  var z = w._config.usePrefixes
    ? " -webkit- -moz- -o- -ms- ".split(" ")
    : ["", ""];
  w._prefixes = z;
  var T = "Moz O ms Webkit",
    E = w._config.usePrefixes ? T.split(" ") : [];
  w._cssomPrefixes = E;
  var k = w._config.usePrefixes ? T.toLowerCase().split(" ") : [];
  w._domPrefixes = k;
  var N = { elem: i("modernizr") };
  Modernizr._q.push(function () {
    delete N.elem;
  });
  var j = { style: N.elem.style };
  Modernizr._q.unshift(function () {
    delete j.style;
  }),
    (w.testAllProps = g),
    (w.testAllProps = h),
    Modernizr.addTest("backdropfilter", h("backdropFilter")),
    Modernizr.addTest("cssfilters", function () {
      if (Modernizr.supports) return h("filter", "blur(2px)");
      var e = i("a");
      return (
        (e.style.cssText = z.join("filter:blur(2px); ")),
        !!e.style.length && (n.documentMode === t || n.documentMode > 9)
      );
    }),
    s(),
    o(C),
    delete w.addTest,
    delete w.addAsyncTest;
  for (var A = 0; A < Modernizr._q.length; A++) Modernizr._q[A]();
  e.Modernizr = Modernizr;
})(window, document);

$(document).ready(function () {
  $("body").scrollspy({
    offset: 190,
  });

  function scrolled() {
    if ($(document).scrollTop() >= 1) {
      $("html").addClass("scrolled");
    } else {
      $("html").removeClass("scrolled");
    }
  }
  scrolled();

  //*************************************************
  // Hide social module when it reaches top of footer
  //*************************************************
  let socialModule = $(".social-module");
  function hideSocialModule() {
    if (
      $(window).scrollTop() +
        ($(window).height() + $("footer").outerHeight()) -
        15 >=
      $(document).height()
    ) {
      socialModule.addClass("offscreen");
    } else {
      socialModule.removeClass("offscreen");
    }
  }
  hideSocialModule();

  //****************
  // Scroll function
  //****************
  $(document).scroll(function () {
    scrolled();
    hideSocialModule();
  });

  gsap.registerPlugin(CSSRulePlugin, ScrollTrigger, ScrollToPlugin);

  let tlNav = gsap.timeline({
    // yes, we can add it to an entire timeline!
    scrollTrigger: {
      trigger: "#content",
      start: "top 50",
      end: "100%",
      markers: false,
      toggleActions: "play pause resume reverse",
    },
  });

  // tlNav
  //   .to("#april6", { x: -95, opacity: 0.5, duration: 0.5 })
  //   .to("#header", { backgroundColor: "rgb(255,255,255,1)" }, "<");

  $("body").append('<div class="navigation-overlay"></div>');
  //   control nav
  $("#nav-icon").click(function (e) {
    e.preventDefault();
    $("#nav-icon-wrapper").toggleClass("active");
    $("#nav-wrapper").toggleClass("open");
    $(".navigation-overlay").fadeToggle();
    $("body").toggleClass("overflow-hidden nav-open");
  });

  // video control
  var video = document.getElementById("modal-video");
  $(".reveal-overlay").click(function (e) {
    e.preventDefault();
    console.log("click");
    // var video = $('.modal-video');
    video.pause();
    video.currentTime = 0;
    // video.load();
  });

  // remove poster image and play video on click
  // $(".video-wrapper").click(function (e) {
  //   var vdo = $(this).find('.vdo'),
  //       overlay = $(this).find(".video-panel_overlay"),
  //       play = $(this).find(".play-trigger");
  //   console.log(vdo);
  //   e.preventDefault();
  //   overlay.hide();
  //   play.hide("slow");
  //   vdo.trigger('play');

  // });

  $(".our-clients_carousel").slick({
    autoplay: true,
    centerMode: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    infinite: true,
    pauseOnHover: false,
    pauseOnFocus: false,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  });
  $(".meet-the-team_carousel").slick({
    autoplay: true,
    autoplaySpeed: 10000,
    dots: true,
    slidesToShow: 4,
    slidesToScroll: 4,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          autoplaySpeed: 7500,
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 768,
        settings: {
          autoplaySpeed: 5000,
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  });

  // $(".docs-carousel").slick({});

  $(".hero-carousel-inner").slick({
    autoplay: true,
    fade: true,
    arrows: false,
    autoplaySpeed: 7000,
    pauseOnHover: false,
    pauseOnFocus: false,
  });

  $(".modaal").modaal({
    after_open: function () {
      console.log("modal open");
      var vdo = $(".modal-content-container");
    },
    before_close: function () {
      console.log("before close");

      console.log(video);
      document.querySelectorAll("video").forEach((vid) => vid.pause());
      document
        .querySelectorAll("video")
        .forEach((vid) => (vid.currentTime = 0));
      $(".video-panel_overlay").show();
      $(".play-trigger").show();
    },
    after_close: function (vdo) {
      console.log("modal closed");
    },
  });

  $(".hero-video .video-wrapper").append(`
  <button data-muted='true' class='sound-control'><span>Sound off</span><i class="fas fa-volume-mute"></i><i class="fas fa-volume-up"></i></button>
  `);

  $(function () {
    //restart hero videos on page load so they're in sync
    $(".hero-video video").currentTime = 0;
    $(".hero-video video").trigger("play");
  });

  $(".hero-video").on("click", ".sound-control", function (e) {
    let videoToControl = $(this).parents(".video-wrapper").find("video");
    if (videoToControl.prop("muted")) {
      videoToControl.prop("muted", false);
      $(this).attr("data-muted", false).children("span").text("Sound on");
    } else {
      videoToControl.prop("muted", true);
      $(this).attr("data-muted", true).children("span").text("Sound off");
    }
    e.preventDefault();
  });

  $(".video-panel").each(function () {
    let poster = $(this).children("video").attr("poster");
    //alert(poster);
    $(this).append(
      `
      
    <div class="inner-overlay" style="background:url(` +
        poster +
        `)"></div>
    `
    );
  });

  $(".video-panel .inner-overlay").on("click", function () {
    $(this).fadeOut().prev("video").trigger("play");
  });

  $(".video-panel video").on("ended", function () {
    $(this).next(".inner-overlay").fadeIn();
  });

  // SMOOTH SCROLLING PLUS OFFSET FOR FIXED NAV

  $('a[href*="#"]')
    // Remove links that don't actually link to anything
    .not('[href="#"]')
    .not('[href="#0"]')
    .on("click", function (event) {
      // Make sure this.hash has a value before overriding default behavior
      if (this.hash !== "") {
        // Store hash
        var hash = this.hash;

        // Using jQuery's animate() method to add smooth page scroll
        // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
        // - 70 is the offset/top margin
        $("html, body").animate(
          {
            scrollTop: $(hash).offset().top - 150,
          },
          800,
          function () {
            // Add hash (#) to URL when done scrolling (default click behavior), without jumping to hash
            if (history.pushState) {
              history.pushState(null, null, hash);
            } else {
              window.location.hash = hash;
            }
          }
        );
        return false;
      } // End if
    });
});
