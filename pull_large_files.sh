#!/bin/bash

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

SOURCE=aprilsix-web01:/var/www/html/pitch-microsites/httpdocs/
DEST="$SCRIPTPATH"

echo "Pulling mp4, mov and pdf files from $SOURCE to $DEST"

rsync -zarv  --prune-empty-dirs --include "*/" --include="*.mp4" --include="*.mov" --include="*.pdf" --exclude="*" "$SOURCE" "$DEST"